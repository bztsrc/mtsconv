Minetest MTS Repository and Converter
=====================================

__NOTE__: this script has been obsoleted by the more advanced [mtsedit](https://gitlab.com/bztsrc/mtsedit), which has similar
batch mode like this, but also has interactive GUI editor too (hint: MTSEdit can import Minecraft .schematic files as well).

I've created a small converter utility and converted some structures with it into [Minetest](https://www.minetest.net) Schematic files
([MTS](https://dev.minetest.net/Minetest_Schematic_File_Format)). Public Domain, use as you like. I did that in the hope that these
structures might be useful in the [Mineclone2 game](https://content.minetest.net/packages/Wuzzy/mineclone2).

```
Minetest Schematics Converter bztsrc@gitlab 2019 - Public Domain

./mtsconv.php [-f] [-c convto.csv|-C convfrom.csv] [-s list] [-n suffix] [-g] <in html|in we|in mts> [-w|-p|-e|-l|out mts]

  -f: force, convert unknown blocks to air
  -c convto.csv: from,to
  -C convfrom.csv: to,from (swaps the csv coloumns)
  -s: comma separated list of selected layers (see -l)
  -n: add suffix to names
  -g: don't mark ground level with rotated air blocks
  -w: write using names found in the html as filenames
  -p: write preview pngs
  -P: write preview pngs, but cut structures in half
  -e: extract multiple schematics from html
  -l: list layers
```

NOTE: to use the converter with other games, mike Minetest Default Game, you'll need a conversion CSV file, that's all.

Dumping Schematics in HTML, WE or MTS files
-------------------------------------------

This is the default operation.
```
./mtsconv.php some_unnamed_wikipedia_page.html
```
or
```
./mtsconv.php witch_hut.we
```
or
```
./mtsconv.php witch_hut.mts
```
This will produce an output similar to this (each layer displays MapNode's param0, param1 and param2 in this order):
```
-------------------------------- witchhut --------------------------------
Map (x: 7 y: 5 (0 5) z: 9):
   0: air                               air
   1: mcl_core:tree                     mcl_core:tree
   2: mcl_core:sprucewood               mcl_core:sprucewood
   3: mcl_crafting_table:crafting_table mcl_crafting_table:crafting_table
   4: mcl_cauldrons:cauldron            mcl_cauldrons:cauldron
   5: mcl_fences:fence                  mcl_fences:fence
   6: mcl_mushrooms:mushroom_red        mcl_mushrooms:mushroom_red
   7: mcl_stairs:stair_sprucewood       mcl_stairs:stair_sprucewood

Layer 0 (probability 127, Ground level):
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 
.1...1.     00 7f 00 00 00 7f 00      20 00 20 20 20 00 20 
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 
.1...1.     00 7f 00 00 00 7f 00      20 00 20 20 20 00 20 
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 
.......     00 00 00 00 00 00 00      20 20 20 20 20 20 20 

Layer 1 (probability 127):
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 
.12221.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.22222.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.22222.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.22222.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.22222.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.12221.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.22222.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
..222..     00 00 7f 7f 7f 00 00      00 00 00 00 00 00 00 

Layer 2 (probability 127):
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 
.12221.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.2.342.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.2...2.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.2...2.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.2...2.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.122.1.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.5...5.     00 7f 00 00 00 7f 00      00 00 00 00 00 00 00 
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 

Layer 3 (probability 127):
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 
.12521.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.2...2.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.6.....     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.......     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.2...2.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.152.1.     00 7f 7f 7f 7f 7f 00      00 00 00 00 00 00 00 
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 

Layer 4 (probability 127):
7777777     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7222227     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7222227     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7222227     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7222227     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7222227     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7222227     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
7777777     7f 7f 7f 7f 7f 7f 7f      00 00 00 00 00 00 00 
.......     00 00 00 00 00 00 00      00 00 00 00 00 00 00 

```
The `air` blocks are displayed as a dot in param0. The mapping above the layers shows which number represents which block types. Without a
conversion, both from and to block types are the same, that's why the type names are repeated in this example.

Generating Preview Images
-------------------------

If you have `php-gd` installed, and you've 32x32 images of bricks in `blockimgs/` directory, then the converter can create preview images of
the structure. The blocks has to be named exactly as they appear in the MTS file, with `.png` extension. For rotation, you can use `_0.png`,
`_1.png`, `_2.png` and `_3.png`, where the number cames from param2. If the rotated block is not found, it fallbacks to `.png`.
```
./mtsconv.php some_unnamed_wikipedia_page_with_possibly_more_blueprints.html -p
```
Please note that because the Minetest Wiki is very limited, many block images are missing (or has a non-isometric picture), so preview
generation is experimental as of now.

If the structure is just a box from the outside, then you can cut it in half to peek inside with uppercase `P`:
```
./mtsconv.php some_unnamed_wikipedia_page_with_possibly_more_blueprints.html -P
```

Converting into MTS
-------------------

This is very similar to preview generation, but has no php extension dependencies. This writes out one MTS file for each blueprint in the HTML.
```
./mtsconv.php some_unnamed_wikipedia_page_with_possibly_more_blueprints.html -w
```
If there's only one blueprint in the HTML, then it makes sense to specify an MTS file name:
```
./mtsconv.php some_unnamed_wikipedia_page_with_possibly_more_blueprints.html witch_hut.mts
```

Extracting Multiple Blueprints from HTML
----------------------------------------

The script is capable of handling more blueprints in an HTML. But it cannot handle variations automatically, therefore it can be
useful to extract blueprints into separate HTML files for further editing before conversion.
```
./mtsconv.php some_unnamed_wikipedia_page_with_possibly_more_blueprints.html -e
```
This works exactly as preview and write, but generates HTML files instead of PNG or MTS.

If a layer is repeated, then it is important to name the `<label>` inside the HTML as "Layer (from) to (to)". Using layer labels
like "Layer (from)-(to)" will confuse the script as it is capable of handling negative layer numbers (below ground level). Otherwise
the label should contain one number, "Layer (num)" to identify which layer that is. There's one exception, the ground level may
not contain a number, as it is interpreter as 0. This seems complicated, but sometimes layers are numbered from 0, and sometimes from
1; if there's an underground part, then layer numbers can be negative as well. If the layer is repeated (like "Layer -7 to -4"), then
the generated MTS will contain repeated layers, but with descending probability.

To help you with this, the converter script can list only the layer names and the numbers as it interpreted those names:
```
./mtsconv.php some_unnamed_wikipedia_page_with_possibly_more_blueprints.html -l
```

This also checks if all block types has a conversion, and lists all blueprints within a HTML. If there are variations, then you should
select the required layers with `-s` and add a name suffix with `-n`, like this:
```
./mtsconv.php -s 0,4,8,11,14,15,16,17,21,25,28,29 -n d Spiral_Staircase.html -l
-------------------------------- Spiral_Staircase_d --------------------------------
[*]   0. layer (0 to 0, string: "Ground Layer, all variants")
[ ]   1. layer (1 to 1, string: "Layer 1a")
[ ]   2. layer (1 to 1, string: "Layer 1b")
[ ]   3. layer (1 to 1, string: "Layer 1c")
[*]   4. layer (1 to 1, string: "Layer 1d")
[ ]   5. layer (2 to 2, string: "Layer 2a")
[ ]   6. layer (2 to 2, string: "Layer 2b")
[ ]   7. layer (2 to 2, string: "Layer 2c")
[*]   8. layer (2 to 2, string: "Layer 2d")
[ ]   9. layer (3 to 3, string: "Layer 3a &amp; 3")
[ ]  10. layer (3 to 3, string: "Layer 3c")
[*]  11. layer (3 to 3, string: "Layer 3d")
[ ]  12. layer (4 to 4, string: "Layer 4a &amp; 4")
[ ]  13. layer (4 to 4, string: "Layer 4c")
[*]  14. layer (4 to 4, string: "Layer 4d")
[*]  15. layer (5 to 5, string: "Layer 5")
[*]  16. layer (6 to 6, string: "Layer 6")
[*]  17. layer (7 to 7, string: "Layer 7")
[ ]  18. layer (8 to 8, string: "Layer 8a")
[ ]  19. layer (8 to 8, string: "Layer 8b")
[ ]  20. layer (8 to 8, string: "Layer 8c")
[*]  21. layer (8 to 8, string: "Layer 8d")
[ ]  22. layer (9 to 9, string: "Layer 9a")
[ ]  23. layer (9 to 9, string: "Layer 9b")
[ ]  24. layer (9 to 9, string: "Layer 9c")
[*]  25. layer (9 to 9, string: "Layer 9d")
[ ]  26. layer (10 to 10, string: "Layer 10a &amp; 10")
[ ]  27. layer (10 to 10, string: "Layer 10c")
[*]  28. layer (10 to 10, string: "Layer 10d")
[*]  29. layer (11 to 11, string: "Layer 11")
```
This selection works with preview, write MTS and extract as well.

Converting Types
----------------

It is also possible to convert the block type mapping in an MTS file (for example from Minetest Game to MineClone2):
```
./mtsconv.php -c convert.csv badmapping.mts goodmapping.mts
```
Here the `convert.csv` is a plain simple comma-separated-values text file, where each line has two type names separated by a comma.
The first coloumn is the `from` type, and the second coloumn is the `to` type.

Normally if a mapping is missing and the input file is an MTS, then it will simply not convert that type. In other words, for MTS
files it is enough if the CSV contains only the mappings that has to be converted. But if the input is an HTML file, then the CSV
is mandatory, and a missing mapping is a fatal error. In other words, with HTML inputs, the CSV must list all the possible types.
To overcome the missing mapping issue, you can force the conversion:
```
./mtsconv.php -f -c convert.csv some_unnamed_wikipedia_page.html output.mts
```
However it generates type names that won't be understood by Minetest. If the conversion CSV is not given on the command line,
it defaults to `html2mcl.csv` in the same directory where the script resides (but having a CSV is still mandatory for HTMLs).

Block Rotation
--------------

If the HTML has a specific CSS style on the blocks "transform:rotate", then the script will parse that and it will set param2 accordingly.
However it is often not specified, so manually rotating some blocks may be needed. On the ground level (because there's no other way to
mark it in an MTS file), the "air" blocks will have the param2 of 32, which is in invalid rotation. This should not interfere with any real
rotation because the Minetest Engine interprets param2 always as param2 & 0x1F. Also there's no point in rotating the "air" block, and it
is neither a light source nor a liquid, so this should be fine (at least //mtschemplace accepts it without problems). If you don't want to
mark the ground level, you can pass `-g` flag to the converter.

Block Probability
-----------------

You can't change each block's probability, but the script does a good job figuring that out on it's own. By default each block has
the probability of 127, except for the "air", which is 0. But if "air" blocks are inside a room, then the script assigns probability
of 127 to those blocks too.

Known Issues
------------

Most models has empty param2. It is very difficult to figure out what's that for; it is not set for light sources neither for liquids. For
normal blocks, the rotation is only set if the original dataset had that information (most often not), so may need to rotate blocks.


That's all.

bzt
