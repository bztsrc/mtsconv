#!/usr/bin/php
<?php
/**
 * Minetest MTS converter by bzt, Copyright (c) 2019 Public Domain
 */
function calcprob($s) {
    global $x, $y, $z, $layers, $par1s, $par2s, $mark, $miny;
    for($i = 0; $i < $y[$s]; $i++) {
        for($j = 1; $j < $z[$s]-1; $j++)
            for($k = 1; $k < $x[$s]-1; $k++) {
                if(!$par1s[$s][$i][$j][$k] && (
                    ($par1s[$s][$i][$j-1][$k] && ($par1s[$s][$i][$j+1][$k] || @$par1s[$s][$i][$j+2][$k])) ||
                    ($par1s[$s][$i][$j][$k-1] && ($par1s[$s][$i][$j][$k+1] || @$par1s[$s][$i][$j][$k+2]))))
                        $par1s[$s][$i][$j][$k] = 127;
            }
        for($j = 1; $j < $z[$s]-1; $j++)
            for($k = 1; $k < $x[$s]-1; $k++) {
                if(!$par1s[$s][$i][$j][$k] &&
                    ($par1s[$s][$i][$j-1][$k] || $par1s[$s][$i][$j][$k-1]))
                        $par1s[$s][$i][$j][$k] = 1;
            }
        do {
            $w = 0;
            for($j = 1; $j < $z[$s]-1; $j++)
                for($k = 1; $k < $x[$s]-1; $k++) {
                    if($par1s[$s][$i][$j][$k]==1 &&
                        (!$par1s[$s][$i][$j-1][$k] || !$par1s[$s][$i][$j+1][$k] || !$par1s[$s][$i][$j][$k-1] || !$par1s[$s][$i][$j][$k+1])) {
                            $par1s[$s][$i][$j][$k] = 0;
                            $w = 1;
                    }
                }
        } while($w);
        for($j = 1; $j < $z[$s]-1; $j++)
            for($k = 1; $k < $x[$s]-1; $k++)
                if($par1s[$s][$i][$j][$k]==1) $par1s[$s][$i][$j][$k] = 127;
        /* mark ground level, set param2 to 0x20 for air blocks */
        if($mark && (($miny[$s] < 0 && $i == -$miny[$s]) || ($miny[$s] >=0 && !$i))) {
            for($j = 0; $j < $z[$s]; $j++)
                for($k = 0; $k < $x[$s]; $k++)
                    if(!$layers[$s][$i][$j][$k]) $par2s[$s][$i][$j][$k] = 32;
        }
    }
}
/* parse command line and get data */
$doexp = $dolist = $i = $force = 0; $p = $o = $defc = $mark = 1; $suff = "";
$c = dirname(__FILE__)."/html2mcl.csv";
if(@$_SERVER['argv'][$p]=="-f") { $force = 1; $p++; }
if(@$_SERVER['argv'][$p]=="-s") {
    $sel = explode(",",@$_SERVER['argv'][$p+1]);
    $p += 2;
}
if(@$_SERVER['argv'][$p]=="-n") {
    $suff = @$_SERVER['argv'][$p+1];
    $p += 2;
}
if(@$_SERVER['argv'][$p]=="-g") { $mark = 0; $p++; }
if(@$_SERVER['argv'][$p]=="-c" || @$_SERVER['argv'][$p]=="-C") {
    if($_SERVER['argv'][$p]=="-C") { $i = 1; $o = 0; }
    $c = @$_SERVER['argv'][$p+1];
    $in = @$_SERVER['argv'][$p+2];
    $out = @$_SERVER['argv'][$p+3];
    $defc = 0;
} else if(@$_SERVER['argv'][$p]=="-e" || @$_SERVER['argv'][$p]=="-l") {
    $in = @$_SERVER['argv'][$p+1];
    $out = "";
    if($_SERVER['argv'][$p]=="-e") $doexp = 1; else $dolist = 1;
} else {
    $in = @$_SERVER['argv'][$p];
    $out = @$_SERVER['argv'][$p+1];
}
if($out == "-e") { $doexp = 1; $out = ""; }
if($out == "-l") { $dolist = 1; $out = ""; }
if(empty($in)) die(
    "Minetest Schematics Converter bztsrc@gitlab 2019 - Public Domain\n\n".
    "./mtsconv.php [-f] [-c convto.csv|-C convfrom.csv] [-s list] [-n suffix] [-g] <in html|in we|in mts> [-w|-p|-e|-l|out mts]\n\n".
    "  -f: force, convert unknown blocks to air\n".
    "  -c convto.csv: from,to\n  -C convfrom.csv: to,from (swaps the csv coloumns)\n".
    "  -s: comma separated list of selected layers (see -l)\n".
    "  -n: add suffix to names\n".
    "  -g: don't mark ground level with rotated air blocks\n".
    "  -w: write using names found in the html as filenames\n".
    "  -p: write preview pngs\n".
    "  -P: write preview pngs, but cut structures in half\n".
    "  -e: extract multiple schematics from html\n".
    "  -l: list layers\n".
    "\n");
$convert = ["" => "air"];
$f = fopen($c, "r");
if($f) {
    while(!feof($f)) {
        $d = fgetcsv($f);
        if(!empty(trim($d[$i])))
            $convert[strtolower(trim($d[$i]))] = trim($d[$o]);
    }
    fclose($f);
}
/* read input */
$data = file_get_contents($in);
if(empty($data)) die();
if(substr($data,0,4)=="MTSM" && $data[4]==chr(0) && $data[5]==chr(4)) {
    /* MTS */
    $header = unpack("nx/ny/nz", $data, 6); $data = substr($data, 12);
    $title[0] = substr($in, 0, strlen($in)-4);
    $x[0] = $header['x'];
    $y[0] = $maxy[0] = $header['y']; $miny[0] = 0;
    $z[0] = $header['z'];
    $sliceprob[0] = unpack("C".$header['y'], $data); $data = substr($data, $header['y']);
    $numid = unpack("n", $data)[1]; $data = substr($data, 2);
    if($numid < 1) die("ERROR: corrupt file\n");
    $hmap[0] = [];
    for($i = 0; $i < $numid; $i++) {
        $strlen = unpack("n", $data)[1]; $data = substr($data, 2);
        if($strlen < 1) die("ERROR: corrupt file\n");
        $hmap[0][substr($data, 0, $strlen)] = $i; $data = substr($data, $strlen);
    }
    $map[0] = [];
    foreach($hmap[0] as $k=>$v) {
        if(empty($convert[strtolower($k)])) { $map[0][$k] = $v; if(!$defc) echo("WARNING: no conversion for '$k', using identity\n"); }
        else $map[0][$convert[strtolower($k)]] = $v;
    }
    $data = gzuncompress($data);
    $nodecnt = $x[0] * $y[0] * $z[0];
    $param0 = unpack("n".$nodecnt, $data); $data = substr($data, $nodecnt * 2);
    $param1 = unpack("C".$nodecnt, $data); $data = substr($data, $nodecnt);
    $param2 = unpack("C".$nodecnt, $data); $data = substr($data, $nodecnt);
    if(strlen($data)) echo("WARNING: unexpected bytes at end\n");
    for($i = 0; $i < $z[0]; $i++)
        for($j = 0; $j < $y[0]; $j++)
            for($k = 0; $k < $x[0]; $k++) {
                $layers[0][$j][$i][$k] = $param0[$i*$x[0]*$y[0] + $j*$x[0] + $k + 1];
                $par1s[0][$j][$i][$k] = $param1[$i*$x[0]*$y[0] + $j*$x[0] + $k + 1];
                $par2s[0][$j][$i][$k] = $param2[$i*$x[0]*$y[0] + $j*$x[0] + $k + 1];
            }
} else if(strpos(substr($data,0,128), ":return") !== false) {
    /* WorldEdit */
    $title[0] = substr($in, 0, strlen($in)-4);
    $data = trim(str_replace("[\"", "\"", str_replace("\"]", "\"", str_replace(" = ", ":", substr($data,strpos($data,"{"))))));
    if(substr($data,0,2)=="{{") $data = "[".substr($data,1,strlen($data)-2)."]";
    $data = json_decode($data, true);
    /* get dimensions and map */
    $x[0] = $y[0] = $z[0] = 0; $miny[0] = 255; $maxy[0] = -255;
    $hmap[0] = ["air" => 0]; $i = 1;
    foreach($data as $d) {
        if($d['x']+1 > $x[0]) $x[0] = $d['x']+1;
        if($d['z']+1 > $z[0]) $z[0] = $d['z']+1;
        if($d['y'] > $maxy[0]) $maxy[0] = $d['y'];
        if($d['y'] < $miny[0]) $miny[0] = $d['y'];
        if(!isset($hmap[0][$d['name']])) $hmap[0][$d['name']] = $i++;
    }
    $y[0] = $maxy[0] - $miny[0] + 1;
    $map[0] = [];
    foreach($hmap[0] as $k=>$v) {
        if(empty($convert[strtolower($k)])) { $map[0][$k] = $v; if(!$defc) echo("WARNING: no conversion for '$k', using identity\n"); }
        else $map[0][$convert[strtolower($k)]] = $v;
    }
    $sliceprob[0] = [];
    for($i = 0; $i < $y[0]; $i++) {
        $sliceprob[0][$i+1] = 127;
        for($j = 0; $j < $z[0]; $j++)
            for($k = 0; $k < $x[0]; $k++)
                $layers[0][$i][$j][$k] = $par1s[0][$i][$j][$k] = $par2s[0][$i][$j][$k] = 0;
    }
    /* get block layers */
    foreach($data as $d) {
        $layers[0][$d['y']-$miny[0]][$d['z']][$d['x']] = $hmap[0][$d['name']];
        $par1s[0][$d['y']-$miny[0]][$d['z']][$d['x']] = !empty($d["param1"]) ? intval($d["param1"]) : 127;
        $par2s[0][$d['y']-$miny[0]][$d['z']][$d['x']] = !empty($d["param2"]) ? intval($d["param2"]) : 0;
    }
    /* fill in probability */
    calcprob(0);
} else {
    /* HTML */
    for($s = $L = 0; strpos($data, "<div class=\"layered-blueprint\"")!==FALSE; $s++) {
        $data = substr($data, strpos($data, "<div class=\"layered-blueprint\""));
        if(empty($title[$s])) {
            preg_match("|name=\"([^\"]+)|", $data, $m);
            $title[$s] = trim(str_ireplace("blueprint", "", $m[1])).($suff?"_".$suff:"");
        }
        if($doexp) {
            $fn = str_replace([":","/"," "],"_",$title[$s]).".html";
            echo("writing extracted schematics ".$fn."\n");
            file_put_contents($fn,substr($data, 0, strpos($data, "</div></div>")+13));
        }
        if($dolist)
            echo("-------------------------------- ".$title[$s]." --------------------------------\n");
        $data = substr($data, 32);
        $lines=explode("<input type=\"radio\"", substr($data, 0, strpos($data, "</div></div>")));
        $hmap[$s][""] = 0;
        /* get dimensions */
        $x[$s] = $z[$s] = 0; $miny[$s] = 255; $maxy[$s] = -255; $j = $L;
        foreach($lines as $layer) {
            preg_match("|<label[^>]*?>([^\-0-9]+([\-0-9]+)[^\,\(\-0-9<]*([\-0-9]*)[^<]*?)|", $layer, $m);
            if(!count($m)) continue;
            if($m[2] == "-" || $m[2] == "") $m[2] = 0;
            if($m[3] == "-" || (empty($m[3]) && $m[3] != "0")) $m[3] = $m[2];
            if($dolist) echo("[".(empty($sel)||in_array($j,$sel)?"*":" ")."] ".sprintf("%3d",$j).". layer (".$m[2]." to ".$m[3].", string: \"".explode("<",$m[1])[0]."\")\n");
            if(!empty($sel)&&!in_array($j,$sel)) { $j++; continue; }
            if(intval($m[2]) < $miny[$s]) $miny[$s] = intval($m[2]);
            if(intval($m[3]) > $maxy[$s]) $maxy[$s] = intval($m[3]);
            preg_match_all("|<tr.*?<\/tr>|ms", $layer, $m, PREG_SET_ORDER);
            if(!count($m)) continue;
            if(count($m) > $z[$s]) $z[$s] = count($m);
            foreach($m as $row) {
                preg_match_all("|<td.*?<\/td>|ms", $row[0], $c, PREG_SET_ORDER);
                foreach($c as $i=>$d) {
                    preg_match("|<a[^\"]+?\"\/([^\"]+)|", $d[0], $e);
                    if(!empty($e[1])) {
                        if(!isset($hmap[$s][$e[1]])) $hmap[$s][$e[1]] = count($hmap[$s]);
                        if($i + 1 > $x[$s]) $x[$s] = $i + 1;
                    }
                }
            }
            $j++;
        }
        $y[$s] = $maxy[$s] - $miny[$s] + 1;
        $w = $i = 0; $map[$s] = [];
        foreach($hmap[$s] as $k=>$v) {
            if(empty($convert[strtolower($k)])) {
                if($force) { $map[$s]["unknown:".strtolower($k)] = $v; }
                else { $w = 1; echo("ERROR: no conversion for '$k'\n"); }
            } else {
                if(!isset($map[$s][$convert[strtolower($k)]]))
                    $map[$s][$convert[strtolower($k)]] = $i++;
                $hmap[$s][$k] = $map[$s][$convert[strtolower($k)]];
            }
        }
        if($w) die("ERROR: missing conversions\n");
        $sliceprob[$s] = [];
        for($i = 0; $i < $y[$s]; $i++)
            for($j = 0; $j < $z[$s]; $j++)
                for($k = 0; $k < $x[$s]; $k++)
                    $layers[$s][$i][$j][$k] = $par1s[$s][$i][$j][$k] = $par2s[$s][$i][$j][$k] = 0;
        /* get block layers */
        $last = 0;
        foreach($lines as $layer) {
            preg_match("|<label[^>]*?>[^\-0-9]+([\-0-9]+)[^\,\(\-0-9<]*([\-0-9]*)|", $layer, $l);
            if(!count($l)) continue;
            if(!empty($sel) && !in_array($L,$sel)) { $L++; continue; }
            if($l[1] == "-" || $l[1] == "") $l[1] = 0; else $l[1] = intval($l[1]);
            if($l[2] == "-" || (empty($l[2]) && $l[2] != "0")) $l[2] = intval($l[1]);
            $l[1] -= $miny[$s]; $l[2] -= $miny[$s];
            preg_match_all("|<tr.*?<\/tr>|ms", $layer, $m, PREG_SET_ORDER);
            if(!count($m)) continue;
            $layer = []; $par2 = [];
            foreach($m as $i=>$row) {
                preg_match_all("|<td.*?<\/td>|ms", $row[0], $c, PREG_SET_ORDER);
                foreach($c as $j=>$d) {
                    preg_match("|<a[^\"]+?\"\/([^\"]+)|", $d[0], $e);
                    if(!empty($e[1])) {
                        $layer[$i][$j] = $hmap[$s][$e[1]];
                        preg_match("|rotate\(([0-9]+)|", $d[0], $e);
                        switch(intval(@$e[1])) {
                            case 90: $par2[$i][$j] = 2; break;
                            case 180: $par2[$i][$j] = 1; break;
                            case 270: $par2[$i][$j] = 3; break;
                        }
                    }
                }
            }
            if($l[1] < $last) die("ERROR: duplicate layer ".$l[1]."\n");
            if($l[1] != $last) die("ERROR: missing layer ".$last."\n");
            for($i = $l[1], $j = 127; $i <= $l[2]; $i++, $j -= intval(64/($l[2] - $l[1] + 1))) {
                $sliceprob[$s][$i+1] = ($miny[$s] < 0 && $i == -$miny[$s]) || ($miny[$s] >=0 && !$i) ? 127 : $j;
                foreach($layer as $Z=>$row)
                    foreach($row as $X=>$t) {
                        $layers[$s][$i][$Z][$X] = $t;
                        $par1s[$s][$i][$Z][$X] = 127;
                        $par2s[$s][$i][$Z][$X] = intval(@$par2[$Z][$X]);
                    }
            }
            $last = $i;
            $L++;
        }
        /* fill in probability */
        calcprob($s);
    }
    if($doexp) die("HTML schematics extracted.\n");
    if($dolist) die();
}
if(empty($out)) {
    /* print out to stdout */
    foreach($layers as $s=>$v) {
        echo("-------------------------------- ".$title[$s]." --------------------------------\n");
        echo("Map (x: ".$x[$s]." y: ".$y[$s]." (".$miny[$s]." ".$maxy[$s].") z: ".$z[$s]."):\n");
        $i = 0; foreach($hmap[$s] as $k=>$v) if(strlen($k) > $i) $i=strlen($k);
        $m = array_flip($hmap[$s]);
        foreach($map[$s] as $k=>$v)
            echo(sprintf("%4d",$v).": ".sprintf("%-".($i+1)."s",$m[$v]).$k."\n");
        echo("\n");
        $m = array_flip($map[$s]);
        foreach($layers[$s] as $y=>$layer) {
            $g = 0;
            foreach($par2s[$s][$y] as $row)
                foreach($row as $c)
                    if($c == 32) { $g = 1; break; }
            echo("Layer $y (probability ".$sliceprob[$s][$y+1].($g?", Ground level":"")."):\n");
            foreach($layer as $z=>$row) {
                foreach($row as $col)
                    echo($m[$col]=="air"?(count($map[$s])>15?".. ":"."):sprintf(count($map[$s])>15?"%02x ":"%1x",$col));
                echo("     ");
                foreach($par1s[$s][$y][$z] as $col)
                    echo(sprintf("%02x ",$col));
                echo("     ");
                foreach($par2s[$s][$y][$z] as $col)
                    echo(sprintf("%02x ",$col));
                echo("\n");
            }
            echo("\n");
        }
    }
} else if($out == "-p" || $out == "-P") {
    /* write out preview image(s) */
    if(!extension_loaded("gd")) dl("gd");
    $miss = [];
    foreach($layers as $s=>$v) {
        $m = array_flip($map[$s]);
        $mx = $x[$s] > $z[$s] ? $x[$s] : $z[$s];
        $ix = $sx = $mx * 24 + 16; $iy = ($mx + $y[$s] - 1) * 16;
        $im = imagecreatetruecolor($ix, $iy);
        imagefilledrectangle($im, 0, 0, $ix, $iy, imagecolorallocatealpha($im, 255, 255, 255, 255));
        $miy = $iy; $may = 0; $iy = ($y[$s] - 1) * 16; $ix = $ix/2 - 16;
        foreach($layers[$s] as $Y=>$layer) {
            foreach($layer as $Z=>$row) {
                foreach($row as $X=>$col) {
                    if($m[$col]=="air") continue;
                    if($out == "-P" && $X >= $x[$s] * ($z[$s] - round($Z * $Y / $y[$s])) / $z[$s]) continue;
                    if($par2s[$s][$Y][$Z][$X] < 4) $bm = @imagecreatefrompng(dirname(__FILE__)."/blockimgs/".$m[$col]."_".$par2s[$s][$Y][$Z][$X].".png");
                    if(!$bm) $bm = @imagecreatefrompng(dirname(__FILE__)."/blockimgs/".$m[$col].".png");
                    if(!$bm) {
                        $miss[$m[$col]] = 1;
                        $bm = imagecreatefrompng(dirname(__FILE__)."/blockimgs/unknown.png");
                    }
                    $j = $iy+$Z*7+$X*7; if($j < $miy) $miy = $j; if($j > $may) $may = $j;
                    imagegammacorrect($bm, 1.0, 1.77 - (($mx - ($X < $Z ? $X : $Z))/$mx*0.75));
                    imagecopy($im, $bm, $ix-$Z*12+$X*12,$j,0,0,32,32);
                    imagedestroy($bm);
                    unset($bm);
                }
            }
            $iy -= 16;
        }
        /* don't trust imagecropauto, better if we do it ourselves */
        $im2 = imagecreatetruecolor($sx, $may-$miy+32);
        imagecopy($im2, $im, 0, 0, 0, $miy, $sx, $may-$miy+32);
        imagedestroy($im);
        /* magic to shrink preview image size and make it transparent background */
        $fn = str_replace([":","/"," "],"_",$title[$s]).".png";
        echo("writing preview ".$fn."\n");
        $bg = imagecolorallocatealpha($im2, 255, 255, 255, 255);
        imagetruecolortopalette($im2, 0, 255);
        imagecolortransparent($im2, $bg);
        imagesavealpha($im2, true);
        imagepng($im2, $fn);
        imagedestroy($im2);
    }
    foreach($miss as $k=>$v)
        echo("WARNING: missing block image for '$k'\n");
} else {
    /* write out MTS file(s) */
    foreach($layers as $s=>$v) {
        $data = "MTSM".pack("nnnn", 4, $x[$s], $y[$s], $z[$s]);
        foreach($sliceprob[$s] as $v) $data .= pack("C", $v);
        $data .= pack("n", count($map[$s]));
        foreach($map[$s] as $m=>$k) $data .= pack("n", strlen($m)).$m;
        $param0 = $param1 = $param2 = "";
        for($i = 0; $i < $z[$s]; $i++)
            for($j = 0; $j < $y[$s]; $j++)
                for($k = 0; $k < $x[$s]; $k++) {
                    $param0 .= pack("n", $layers[$s][$j][$i][$k]);
                    $param1 .= pack("C", $par1s[$s][$j][$i][$k]);
                    $param2 .= pack("C", $par2s[$s][$j][$i][$k]);
                }
        $data .= gzcompress($param0.$param1.$param2);
        $fn = $out == "-w" ? str_replace([":","/"," "],"_",$title[$s]).".mts": $out;
        echo("writing ".$fn." (".strlen($data)." bytes)\n");
        file_put_contents($fn, $data);
    }
}
